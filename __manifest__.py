# Copyright © 2021 Novobi, LLC
# See LICENSE file for full copyright and licensing details.
{
    'name': 'Invoice Website',
    'summary': "Invoices Website",
    'category': 'Invoice',
    "version": "1.0",
    "author": "Yami Nguyen",
    'depends': ['base', 'website'],
    'data': [
        'views/invoice_search_view.xml',
    ],
    'installable': True,
}
