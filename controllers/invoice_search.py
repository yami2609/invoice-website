import werkzeug

from odoo import http, api
from odoo.http import request


class invoice_search(http.Controller):

    @http.route('/invoice', auth="public")
    def index(self, **kw):
        return http.request.render('Invoice_Website.invoice_search_view', {
            'search': False
        })

    @http.route('/invoice/search', auth="public")
    def search(self, **kw):
        x = 'dasdas'
        return http.request.render('Invoice_Website.invoice_search_view', {
            'search': True,
            'no': '12345'
        })
